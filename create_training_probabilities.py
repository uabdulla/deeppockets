from common import *

data = read_source_data()
tags = {tag for entry in data for tag in entry[SourceValue.TAGS]}
tags = list(tags)
tags.sort()

min_time = data[0][SourceValue.TIMESTAMP]
max_time = data[-1][SourceValue.TIMESTAMP]

# noinspection PyTypeChecker
month_count = max_time - min_time

probabilities = [[0.0] * len(tags) for i in range(0, month_count + 1)]

for entry in data:
    # noinspection PyTypeChecker
    entry_month = entry[SourceValue.TIMESTAMP] - min_time
    for i, tag in enumerate(tags):
        if tag in entry[SourceValue.TAGS]:
            probabilities[entry_month][i] = 1.0

write_training_probabilities(tags, min_time, probabilities)
