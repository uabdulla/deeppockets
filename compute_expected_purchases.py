from fractions import Fraction

from common import *

extrapolated_probabilities = read_extrapolated_probabilities()


def get_prob_of_k_events(probabilities, k):
    P = [1] + [0] * k
    for p in probabilities:
        for j in range(k, -1, -1):
            P[j] = p * (P[j - 1] if j > 0 else 0) + (1 - p) * P[j]
    return P[k]


tag_probabilities = {}

for tag, probabilities in extrapolated_probabilities.items():
    probabilities = probabilities[ExtrapolatedProbabilities.PROBABILITIES]
    probabilities = [Fraction(p) for p in probabilities]
    prob_k_of_n = [get_prob_of_k_events(probabilities, k) for k in
                   range(0, len(probabilities) + 1)]
    prob_k_of_n = [float(f.numerator) / float(f.denominator)
                   for f in prob_k_of_n]
    tag_probabilities[tag] = prob_k_of_n

save_expected_purchases(tag_probabilities)
