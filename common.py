import csv
from enum import Enum, unique

import os

EXTRAPOLATION_LEN = 12
DATA_BASE_DIR = 'data/'
SOURCE_FILE_PATH = DATA_BASE_DIR + 'source.csv'
TRAINING_PROBABILITIES_FILE_PATH = DATA_BASE_DIR + 'training_probabilities.csv'
EXTRAPOLATED_PROBABILITIES_FILE_PATH = \
    DATA_BASE_DIR + 'extrapolated_probabilities.csv'
TRAINING_COSTS_FILE_PATH = DATA_BASE_DIR + 'training_costs.csv'
EXTRAPOLATED_COSTS_FILE_PATH = DATA_BASE_DIR + 'extrapolated_costs.csv'
EXPECTED_PURCHASES_FILE_PATH = DATA_BASE_DIR + 'expected_purchases.csv'
PRODUCTS_FILE_PATH = DATA_BASE_DIR + 'products.csv'

MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
          'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']


@unique
class SourceValue(Enum):
    DATE = 'date'
    TIMESTAMP = 'timestamp'
    TIMESTAMP_MS = 'timestampMs'
    TIME = 'time'
    DESCRIPTION = 'description'
    COST = 'expense'
    TAGS = 'tags'
    LOCATION = 'location'
    STORE = 'store'


@unique
class TrainingData(Enum):
    TAGS = 'tags'
    TIMESTAMP = 'timestamp'
    PROBABILITIES = 'probabilities'


@unique
class TrainingCost(Enum):
    TAGS = 'tags'
    TIMESTAMP = 'timestamp'
    COSTS = 'costs'


@unique
class ExtrapolatedProbabilities(Enum):
    START_TIMESTAMP = 'start_timestamp'
    PROBABILITIES = 'probabilities'


@unique
class ExtrapolatedCosts(Enum):
    START_TIMESTAMP = 'start_timestamp'
    COSTS = 'costs'


@unique
class Product(Enum):
    TAG = 'tag'
    PRODUCT = 'product'
    PROVIDER = 'provider'
    PARAMETER = 'parameter'
    VALUE = 'value'
    DESCRIPTION = 'description'
    SUB_TAG = 'sub_tag'
    SUB_TAG_TEXT = 'sub_tag_text'
    URL = 'url'


def create_dir_if_not_exist(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)


def get_month(str):
    for i, mon in enumerate(MONTHS):
        if mon.lower() == str.lower():
            return i
    raise Exception("Unsupported month: " + str)


def read_source_data():
    def source_date_to_timestamp(source_date_string):
        """Converts the date from the source CSV file to UTC timestamp

        naive datetime will be considered UTC.
        """
        date_pieces = [x.strip() for x in source_date_string.split(' ')]
        mon = get_month(date_pieces[1])
        year = int(date_pieces[2])
        return year * 12 + mon

    def process_row(row_values):
        date_str = row_values[0]
        timestamp = source_date_to_timestamp(date_str)
        time_str = row_values[1]
        description = row_values[2]
        cost_str = row_values[3]
        cost = float(cost_str)
        tag_str = row_values[4]
        tags = {tag.strip() for tag in tag_str.split(',')}
        location = row_values[5]
        store = row_values[6]
        return {
            SourceValue.TIMESTAMP: timestamp,
            SourceValue.TIMESTAMP_MS: timestamp * 1000,
            SourceValue.DATE: date_str,
            SourceValue.TIME: time_str,
            SourceValue.DESCRIPTION: description,
            SourceValue.COST: cost,
            SourceValue.TAGS: list(tags),
            SourceValue.LOCATION: location,
            SourceValue.STORE: store,
        }

    with open(SOURCE_FILE_PATH, 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        next(reader, None)  # skip header
        data = [process_row(row_values) for row_values in reader]
    data.sort(key=lambda entry: entry[SourceValue.TIMESTAMP])
    return data


def write_training_probabilities(tags, min_time, tag_probabilities):
    create_dir_if_not_exist(DATA_BASE_DIR)
    with open(TRAINING_PROBABILITIES_FILE_PATH, 'w') as csv_file:
        header = ['time'] + tags
        print(*header, sep=',', file=csv_file)
        for i, entry in enumerate(tag_probabilities):
            row = [min_time + i] + entry
            print(*row, sep=',', file=csv_file)


def read_training_probabilities():
    def process_row(row_values):
        return {
            TrainingData.TIMESTAMP: int(row_values[0]),
            TrainingData.PROBABILITIES: [float(value) for value in
                                         row_values[1:]]
        }

    with open(TRAINING_PROBABILITIES_FILE_PATH, 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        header = next(reader)
        tags = header[1:]
        data = [process_row(row_values) for row_values in reader]

    return tags, data


def write_training_costs(tags, min_time, tag_costs):
    create_dir_if_not_exist(DATA_BASE_DIR)
    with open(TRAINING_COSTS_FILE_PATH, 'w') as csv_file:
        header = ['time'] + tags
        print(*header, sep=',', file=csv_file)
        for i, entry in enumerate(tag_costs):
            row = [min_time + i] + entry
            print(*row, sep=',', file=csv_file)


def read_training_costs():
    def process_row(row_values):
        return {
            TrainingCost.TIMESTAMP: int(row_values[0]),
            TrainingCost.COSTS: [float(value) for value in
                                 row_values[1:]]
        }

    with open(TRAINING_COSTS_FILE_PATH, 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        header = next(reader)
        tags = header[1:]
        data = [process_row(row_values) for row_values in reader]

    return tags, data


def write_extrapolated_probabilities(tags, start_time, extensions):
    create_dir_if_not_exist(DATA_BASE_DIR)
    with open(EXTRAPOLATED_PROBABILITIES_FILE_PATH, 'w') as csv_file:
        for tag, extension in zip(tags, extensions):
            output = [tag] + [start_time] + extension
            print(*output, sep=',', file=csv_file)


def read_extrapolated_probabilities():
    extrapolated_data = {}
    with open(EXTRAPOLATED_PROBABILITIES_FILE_PATH, 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for row_values in reader:
            tag = row_values[0]
            timestamp = int(row_values[1])
            probabilities = [float(v) for v in row_values[2:]]
            extrapolated_data[tag] = {
                ExtrapolatedProbabilities.START_TIMESTAMP: timestamp,
                ExtrapolatedProbabilities.PROBABILITIES: probabilities
            }
    return extrapolated_data


def write_extrapolated_costs(tags, start_time, costs):
    create_dir_if_not_exist(DATA_BASE_DIR)
    with open(EXTRAPOLATED_COSTS_FILE_PATH, 'w') as csv_file:
        for tag, extension in zip(tags, costs):
            output = [tag] + [start_time] + extension
            print(*output, sep=',', file=csv_file)


def read_extrapolated_costs():
    extrapolated_data = {}
    with open(EXTRAPOLATED_COSTS_FILE_PATH, 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for row_values in reader:
            tag = row_values[0]
            timestamp = int(row_values[1])
            costs = [float(v) for v in row_values[2:]]
            extrapolated_data[tag] = {
                ExtrapolatedCosts.START_TIMESTAMP: timestamp,
                ExtrapolatedCosts.COSTS: costs
            }
    return extrapolated_data


def save_expected_purchases(probabilities):
    create_dir_if_not_exist(DATA_BASE_DIR)
    with open(EXPECTED_PURCHASES_FILE_PATH, 'w') as csv_file:
        for tag, values in probabilities.items():
            print(tag, *values, sep=',', file=csv_file)


def read_expected_purchases():
    expected_purchases = {}
    with open(EXPECTED_PURCHASES_FILE_PATH, 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for row_values in reader:
            tag = row_values[0]
            probabilities = [float(v) for v in row_values[1:]]
            expected_purchases[tag] = probabilities
    return expected_purchases


def read_products():
    products = []
    with open(PRODUCTS_FILE_PATH, 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        next(reader, None)  # skip header
        for row_values in reader:
            product = {
                Product.TAG: row_values[0],
                Product.PRODUCT: row_values[1],
                Product.PROVIDER: row_values[2],
                Product.PARAMETER: row_values[3],
                Product.VALUE: float(row_values[4]),
                Product.DESCRIPTION: row_values[5],
                Product.SUB_TAG: row_values[6],
                Product.SUB_TAG_TEXT: row_values[7],
                Product.URL: row_values[8]
            }
            products.append(product)
    return products


def max_with_index(iterable):
    iterable = enumerate(iterable)
    max_index, max_value = next(iterable)
    for i, v in iterable:
        if v > max_value:
            max_value = v
            max_index = i
    return max_index, max_value
