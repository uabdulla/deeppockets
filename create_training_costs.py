from common import *

data = read_source_data()
tags = {tag for entry in data for tag in entry[SourceValue.TAGS]}
tags = list(tags)
tags.sort()

min_time = data[0][SourceValue.TIMESTAMP]
max_time = data[-1][SourceValue.TIMESTAMP]

# noinspection PyTypeChecker
day_count = max_time - min_time

nan = float('nan')
costs = [[nan] * len(tags) for i in range(0, day_count + 1)]

for entry in data:
    # noinspection PyTypeChecker
    entry_day = entry[SourceValue.TIMESTAMP] - min_time
    for i, tag in enumerate(tags):
        if tag in entry[SourceValue.TAGS]:
            costs[entry_day][i] = entry[SourceValue.COST]

write_training_costs(tags, min_time, costs)
