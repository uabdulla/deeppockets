import numpy
from common import *

tags, data = read_training_costs()
timestamps = [entry[TrainingCost.TIMESTAMP] for entry in data]
max_time = timestamps[-1]
start_time = max_time + 1
extrapolated_timestamps = [max_time + i
                           for i in range(0, EXTRAPOLATION_LEN)]


def extrapolate(timeseries):
    x = []
    y = []
    for t, v in zip(timestamps, timeseries):
        if v != v:  # ensure value is not NAN
            continue
        x.append(t)
        y.append(v)
    coefficients = numpy.polyfit(x, y, 1)
    line = numpy.poly1d(coefficients)
    return [round(line(t)) for t in extrapolated_timestamps]


extensions = []
for tag_index, tag in enumerate(tags):
    timeseries = [entry[TrainingCost.COSTS][tag_index] for entry in data]
    extrapolation = extrapolate(timeseries)
    extensions.append(extrapolation)

write_extrapolated_costs(tags, start_time, extensions)
