import numpy
from flask_restful import Api, Resource, abort
from flask import Flask

from common import *


def source_to_json(source_entry):
    prepared = {}
    for key, value in source_entry.items():
        prepared[key.value] = value
    return prepared


source_data = read_source_data()
json_data = [source_to_json(entry) for entry in source_data]

extrapolated_probabilities = read_extrapolated_probabilities()
extrapolated_costs = read_extrapolated_costs()

json_extrapolated_data = {}
for tag in extrapolated_probabilities.keys():
    print(tag)
    starting_time = extrapolated_probabilities[tag] \
        [ExtrapolatedProbabilities.START_TIMESTAMP]
    probabilities = extrapolated_probabilities[tag] \
        [ExtrapolatedProbabilities.PROBABILITIES]
    costs = extrapolated_costs[tag][ExtrapolatedCosts.COSTS]
    extrapolation = []
    for i, probability in enumerate(probabilities):
        extrapolation.append({
            'date': starting_time + i,
            'likelihood': probability,
            'cost': costs[i]
        })
    json_extrapolated_data[tag] = extrapolation

expected_purchases = read_expected_purchases()
monthly_cost = {}
json_expected_purchases = {}

for tag, probabilities in expected_purchases.items():
    costs = extrapolated_costs[tag][ExtrapolatedCosts.COSTS]
    avg_cost = numpy.average(costs)

    expectations = []
    for i, prob in enumerate(probabilities):
        expected_cost = avg_cost * i
        expectations.append({
            'number_of_purchases': i,
            'likelihood': prob,
            'expected_cost': expected_cost
        })
    json_expected_purchases[tag] = expectations

    highest_prob_index, highest_prob = max_with_index(probabilities)
    monthly_cost[tag] = round(avg_cost * highest_prob_index)


def compute_product_savings(product):
    tag = product[Product.TAG]
    if tag not in monthly_cost:
        return 0.0
    monthly_tag_cost = monthly_cost[tag]
    return monthly_tag_cost - product[Product.VALUE]


products = read_products()
product_details = []
product_savings = [compute_product_savings(p) for p in products]


def get_best_product_index(tag):
    best_product_index = -1
    for i, product in enumerate(products):
        if product[Product.TAG] != tag:
            continue
        if best_product_index < 0 \
                or product_savings[i] > product_savings[best_product_index]:
            best_product_index = i
    return best_product_index


def product_to_json(product_index):
    product = products[product_index]
    prepared = {}
    for key, value in product.items():
        prepared[key.value] = value
    tag = product[Product.TAG]
    prepared['expenditure'] = monthly_cost[tag]
    prepared['savings'] = product_savings[product_index]
    return prepared


def create_dashboard_json():
    best_product_json = []
    total_savings = 0.0
    for tag in json_extrapolated_data.keys():
        best_product_index = get_best_product_index(tag)
        if best_product_index < 0:
            continue
        json = product_to_json(best_product_index)
        best_product_json.append(json)
        total_savings += product_savings[best_product_index]
    return {
        'best_products': best_product_json,
        'total_savings': total_savings
    }


def create_detail_json(tag):
    best_product_index = get_best_product_index(tag)
    if best_product_index < 0:
        maximum_savings = 0.0
    else:
        maximum_savings = monthly_cost[tag] \
                          - products[best_product_index][Product.VALUE]
    product_json = []
    for i, product in enumerate(products):
        if tag not in product[Product.TAG]:
            continue
        product_json.append(product_to_json(i))
    return {
        'maximum_savings': maximum_savings,
        'products': product_json
    }


class Data(Resource):
    def get(self):
        return {'data': json_data}


class Tags(Resource):
    def get(self):
        return {'tags': list(json_extrapolated_data.keys())}


class ExtrapolatedData(Resource):
    def get(self, tag):
        if tag not in json_extrapolated_data:
            abort(404, message='Tag does not exist')
            return
        extrapolation = json_extrapolated_data[tag]
        return {'extrapolated_data': extrapolation}


class ExpectedCosts(Resource):
    def get(self, tag):
        if tag not in json_expected_purchases:
            abort(404, message='Tag does not exist')
            return
        expectations = json_expected_purchases[tag]
        return {'likely_costs': expectations}


class Dashboard(Resource):
    def get(self):
        return create_dashboard_json()


class Details(Resource):
    def get(selfs, tag):
        if tag not in json_expected_purchases:
            abort(404, message='Tag does not exist')
            return
        return create_detail_json(tag)


app = Flask(__name__)
api = Api(app)
api.add_resource(Data, '/data')
api.add_resource(Tags, '/tags')
api.add_resource(ExtrapolatedData, '/tags/<string:tag>/extrapolatedData')
api.add_resource(ExpectedCosts, '/tags/<string:tag>/expectedCosts')
api.add_resource(Dashboard, '/dashboard')
api.add_resource(Details, '/details/<string:tag>')
app.run(host='0.0.0.0', port=5001)
