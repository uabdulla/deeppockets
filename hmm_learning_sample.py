import hmmlearn.hmm as hmm
import random

# number if different things to observe and their characteristics
symbol_count = 3
symbol_mean = (0.0, 100.0, 200.0)
symbol_sd = (10.0, 10.0, 10.0)

# number of different hidden states to use to represent the pattern
#   seems to work best if it's 2 year's worth of states with each state
#   representing a month
hidden_state_count = 24

# create the things to observe (an annual pattern repeated over a few years)
sequence_symbols = [1, 1, 0, 0, 0, 0, 2, 0, 2, 0, 0, 0] * 100

# create the observations from the things to observe
sequence_emissions = [random.gauss(symbol_mean[i], symbol_sd[i])
                      for i in sequence_symbols]

# create the HMM object
h = hmm.GaussianHMM(n_components=hidden_state_count,
                    random_state=0,
                    tol=0.00001,
                    n_iter=1000,
                    verbose=True)

# convert the observations to the required format to train with
training_data = [[i] for i in sequence_emissions]

# train the HMM with the training data
h.fit(training_data)

# print out relevant details
print(h)
print(h.means_)
print(h.transmat_)

# sample 2 years worth data
samples = h.sample(24)[0]
# display sampled data (should contain same pattern as above)
samples = [(round(s[0]), round(s[0] / 100.0) * 100.0) for s in samples]
print(*samples, sep='\n')
