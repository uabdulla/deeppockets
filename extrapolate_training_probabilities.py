import hmmlearn.hmm as hmm
import numpy

from common import *

tags, data = read_training_probabilities()

extensions = []
max_time = data[-1][TrainingData.TIMESTAMP]
start_time = max_time + 1


def mult(a, b):
    return [i * j for i, j in zip(a, b)]


def extrapolate(probabilities):
    if len(probabilities) < EXTRAPOLATION_LEN * 3:
        raise Exception("Insufficient data: obtained %d, expected at least %d",
                        len(probabilities), EXTRAPOLATION_LEN * 3)
    sequence = [int(i) for i in probabilities]
    sequences = []
    for s in range(0, len(sequence), EXTRAPOLATION_LEN):
        end = s + EXTRAPOLATION_LEN * 2
        if end > len(sequence):
            break
        subsequence = sequence[s:end]
        sequences.append(subsequence)

    h = hmm.MultinomialHMM(n_components=20,
                           init_params='ste',
                           params='ste',
                           tol=0.000001,
                           n_iter=1000,
                           verbose=False)
    # print(*sequences, sep='\n')
    h.fit(sequences)
    # print(h.startprob_)
    # print(h.transmat_)
    # print(h.emissionprob_)
    sampling = h.sample(n_samples=EXTRAPOLATION_LEN * 4)
    extrapolation = [i[0] for i in sampling[0]]
    # print(extrapolation)
    return extrapolation


def extend_sequence(sequence, pattern):
    """
    Given the pattern, try and fit it to the end of the sequence to get the
    extension of the sequence
    :param sequence:
    :param pattern:
    :return:
    """
    if all(i == 1.0 for i in pattern):
        return pattern
    if all(i == 0.0 for i in pattern):
        return pattern

    # Find the average product of the last 'lag' values of the sequence
    # and the first 'lag' values of the pattern
    lags = []
    for lag in range(1, len(pattern)):
        sequence_start = len(sequence) - lag
        sequence_portion = sequence[sequence_start:]
        pattern_portion = pattern[:lag]
        product = mult(sequence_portion, pattern_portion)
        avg_product = numpy.average(product)
        lags.append({'lag': lag, 'product': avg_product})

    # Find the first two maximas of all the average products
    maximas = []
    for i in range(1, len(lags) - 1):
        if (lags[i - 1]['product'] < lags[i]['product'] and
                    lags[i]['product'] > lags[i + 1]['product']):
            maximas.append(lags[i]['lag'])
            if len(maximas) == 2:
                break

    # If not at least two maximas were found, the last
    # time the event occurred is probably too long ago
    if len(maximas) < 2:
        return []

    # Use the second lag as the number of days to go back before today
    #   the remaining autocorrelation is the extension
    extension_lag = maximas[1]
    return pattern[(extension_lag + 1):]


for tag_index, tag in enumerate(tags):
    probabilities = [entry[TrainingData.PROBABILITIES][tag_index]
                     for entry in data]
    extrapolation = extrapolate(probabilities)
    extension = extend_sequence(probabilities, extrapolation)
    if len(extension) > EXTRAPOLATION_LEN:
        extension = extension[:EXTRAPOLATION_LEN]
    extensions.append(extension)

write_extrapolated_probabilities(tags, start_time, extensions)
